﻿# A PowerShell script to get the status of the last build in a TeamCity project
# If there is a new build, download the artifacts as a .zip file.

$username = "administrator"
$password = "Q1w2e3r4t%"
$domain = ""
$TcProjectId="PierwszyProjektKlody_PythonCopyFilesFromOneFolderToAnother"
$serverAddress = "http://localhost"
$command = $serverAddress + "/httpAuth/app/rest/builds/?buildTypeId='" + 
           $TcProjectId + "'&locator=start:0,count:50"
$downloadUrl = "http://localhost/repository/downloadAll/PierwszyProjektKlody_PythonCopyFilesFromOneFolderToAnother/.lastFinished"
$downloadFile = "C:\Temp\PierwszyProjektKlody_PythonCopyFilesFromOneFolderToAnother"
$buildnumber = 0

If ( Test-Path -Path "TCdownload.txt" )
{
 $buildnumber = Get-Content -Path "TCdownload.txt"
 Write-Host "Old build number = " $buildnumber
}

function Execute-HTTPPostCommand() {
    param(
        [string] $target = $null
    )
 
    $request = [System.Net.WebRequest]::Create($target)
    $request.PreAuthenticate = $true
    $request.Method = "GET"
    $request.Credentials = new-object system.net.networkcredential($username, $password)
    $response = $request.GetResponse()
    $sr = [Io.StreamReader]($response.GetResponseStream())
    $xmlout = $sr.ReadToEnd()
    return $xmlout;
}
 
$xml = [xml]$(Execute-HTTPPostCommand $command)
$xml2 = $xml.builds.build | where { $_.buildTypeId.equals($TcProjectId) }

Write-Host "TeamCity " $TcProjectId $xml2[0].number $xml2[0].status

If ($xml2[0].number -gt $buildnumber)
{
 $downloadFile += $xml2[0].number + ".zip"
 Write-Host "Downloading $downloadFile ..."
 $xml2[0].number | Out-File -FilePath "TCdownload.txt"
 $wc = New-Object System.Net.WebClient
 $wc.Credentials = new-object System.Net.NetworkCredential($username, $password, $domain)
 $wc.DownloadFile($downloadUrl, $downloadFile)
}

Write-Host "Done !"
#Start-Sleep -s 10