﻿# konfiguracja login i hasło
$username = "administrator"
$password = "Q1w2e3r4t%"
$domain = ""
$serverAddress = "http://localhost"
$file_path = ".\xml.xml"


# adres rest pobrania konfiguracji buildów
$buildTypes = $serverAddress + "/httpAuth/app/rest/buildTypes"

#adres pobrania listy projektów
$projects = $serverAddress + "/httpAuth/app/rest/projects"

#funkcja pobrania danych z serwera
function Execute-HTTPPostCommand() {
    param(
        [string] $target = $null
    )
 
    $request = [System.Net.WebRequest]::Create($target)
    $request.PreAuthenticate = $true
    $request.Method = "GET"
    $request.Credentials = new-object system.net.networkcredential($username, $password)
    $response = $request.GetResponse()
    $sr = [Io.StreamReader]($response.GetResponseStream())
    $xmlout = $sr.ReadToEnd()
    return $xmlout;
}

#pobranie do zmiennej xml listy projektow
$xml_projects = [xml]$(Execute-HTTPPostCommand $projects)
#$xml_projects.projects.project | % { Write-Host "Project id :"  $_.id }


#pobranie do zmienenj xml listy konfiguracji buildow  
$xml_buildTypes = [xml]$(Execute-HTTPPostCommand $buildTypes)


#######################################################################
#petla wylistowania wszystkich konfiguracji buildow
foreach ($TcProject in $xml_buildTypes.buildTypes.buildType) {

    # wyswietlenie id projektu oraz nazwy konfiguracji buildu
    $xml_projects.projects.project | % { if ($TcProject.projectId -eq $_.id)  { Write-Host "Projet id :" $_.id " Build configuration :" $TcProject.name }  }  

    #pobranie do zmienne xml listy parametrow z  konfiguracji buildu
    $xml_param = [xml]$(Execute-HTTPPostCommand ($serverAddress + "/httpAuth/app/rest/buildTypes/" + $TcProject.id + "/parameters"))
    
    #petla wyswietlenia listy parametwór
    Foreach ($property in $xml_param.properties.property) { 
        Write-Host  $property.name "          " $property.value 
    }
}#######################################################################


