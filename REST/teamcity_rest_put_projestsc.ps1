﻿# konfiguracja login i hasło
$username = "administrator"
$password = "Q1w2e3r4t%"
$domain = ""
$serverAddress = "http://localhost"

$xml_projects_backup_file = ".\xml_projects_backup_file.xml"
$xml_buildTypes_backup_file = ".\xml_buildTypes_backup_file.xml"
$xml_param_backup_file = ".\xml_param_backup_file.xml"


# adres rest pobrania konfiguracji buildów
$buildTypes = $serverAddress + "/httpAuth/app/rest/buildTypes"

#adres pobrania listy projektów
$projects = $serverAddress + "/httpAuth/app/rest/projects"


#funkcja pobrania danych z serwera
function Execute-HTTPPostCommand() {
    param(
        [string] $target = $null,
        [string] $urlmethod = $null,
        [string] $payload = $null         
    )
 
    $request = [System.Net.WebRequest]::Create($target)
    $request.PreAuthenticate = $true
    $request.Method = $urlmethod
    $request.Headers.Add("Origin", "http://localhost")
    $request.ContentType = "application/xml"  
    $request.Credentials = new-object system.net.networkcredential($username, $password)
 
     
    if ($urlmethod -eq "PUT") {   
    
        $request.ContentType = "text/plain"
        $PostStr = [system.Text.Encoding]::UTF8.GetBytes($payload)
          
        $requestStream = $request.GetRequestStream()
        $requestStream.Write($PostStr, 0, $PostStr.length)
        $requestStream.Close()             
    }
       
    $response = $request.GetResponse()   
    $sr = [Io.StreamReader]($response.GetResponseStream())
    $xmlout = $sr.ReadToEnd()
      
    return $xmlout;
    
}

#pobranie do zmiennej xml listy projektow
$xml_projects = [xml]$(Execute-HTTPPostCommand $projects "GET")
Set-Content $xml_projects_backup_file $xml_projects

#$xml_projects.projects.project | % { Write-Host "Project id :"  $_.id }


#pobranie do zmienenj xml listy konfiguracji buildow  
$xml_buildTypes = [xml]$(Execute-HTTPPostCommand $buildTypes "GET")
Set-Content $xml_buildTypes_backup_file $xml_buildTypes

#wyswietlenie listy projektow
$xml_projects.projects.project | % {Write-Host $_.id}
#wybranie konkretnego projektu
$selected_project = Read-Host -Prompt "Select project "

#Wylistowanie wszystkich konfiguracji dla wybranego projektu 
$xml_buildTypes.buildTypes.buildType | % { if ($_.projectId -eq $selected_project) {Write-Host $_.id} }

#wybranie konkretnej konfiguracji dla danego projektu
$selected_configuration = Read-Host -Prompt "Select configuration "

#pobranie do zmienne xml listy parametrow z  konfiguracji buildu
$xml_param = [xml]$(Execute-HTTPPostCommand ($serverAddress + "/httpAuth/app/rest/buildTypes/" + $selected_configuration + "/parameters") "GET")
Set-Content $xml_param_backup_file $xml_param

 
$xml_param.properties.property | % { Write-Host $_.name "   "$_.value  }


#######################################################################
#petla wylistowania wszystkich konfiguracji buildow
foreach ($TcProject in $xml_buildTypes.buildTypes.buildType) {

    # wyswietlenie id projektu oraz nazwy konfiguracji buildu
    $xml_projects.projects.project | % { if (($TcProject.projectId -eq $_.id) -and ($TcProject.projectId -eq $selected_project) ) { Write-Host "Projet id :" $_.id " Build configuration :" $TcProject.name }  }  

    #pobranie do zmienne xml listy parametrow z  konfiguracji buildu
    $xml_param = [xml]$(Execute-HTTPPostCommand ($serverAddress + "/httpAuth/app/rest/buildTypes/" + $TcProject.id + "/parameters") "GET")
    
    #petla wyswietlenia listy parametwór
    Foreach ($property in $xml_param.properties.property) {
        #wyswietlenie wszystkich zmiennych i wartosci z projektu 
        Write-Host  $property.name "          " $property.value
        if ( $property.name -eq "git.working.directory" ) { 
            #$property.value = "/home/crisku/%system.agent.name%"

            $TcProject_projectId = $TcProject.id
            $property_name = $property.name
            $value = "/home/crisk/%system.agent.name%"          
            $put_command = $serverAddress+"/httpAuth/app/rest/buildTypes/"+$TcProject_projectId+"/parameters/"+$property_name+"/value"
            
            Execute-HTTPPostCommand $put_command "PUT" $value
            
            #Write-Host  $property.name "          " $property.value
        }
    }
}#######################################################################



'
https://jonlabelle.com/snippets/view/powershell/send-a-json-http-api-request-in-powershell
https://wilsonmar.github.io/powershell-rest-api/
'