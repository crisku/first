#!/usr/bin/python

from ansible.module_utils.basic import *
import os

def main():

    host_name_fields = {
        "host_name": {"default": True, "type": "str"},
        "host_is_alive": {"default": True, "type": "str"}
    }
    
    module = AnsibleModule(argument_spec=host_name_fields)
    
    
    response = os.system("ping -c 1 " + module.params["host_name"])
    
    #and then check the response...
    if response == 0:
        module.params.update({"host_is_alive": "is up!"})  #print hostname, 'is up!'
        module.exit_json(changed=True, meta=module.params)

    else:
        module.params.update({"host_is_alive": "is down!"})  #print hostname, 'is down!'
        module.exit_json(changed=False, meta=module.params)


    
    #module.exit_json(changed=True, meta=module.params)


if __name__ == '__main__':
    main()