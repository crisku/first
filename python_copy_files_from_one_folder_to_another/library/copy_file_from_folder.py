#!/usr/bin/python

from ansible.module_utils.basic import *
import shutil
import os

def main():

    fields = {
        "sources": {"default": True, "type": "str"},
        #"mask": {"default": True, "type": "str"},
        "destenation": {"default": True, "type": "str"},
        #"overwrite": {"default": True, "type": "str"}        
    }
    
    module = AnsibleModule(argument_spec=fields)

    for root, dirs, files in os.walk(module.params["sources"]):
        for filename in files:
            result = shutil.copyfile(str(module.params["sources"] + filename), str(module.params["destenation"] + filename))
            
    failed_var=False
            
    module.exit_json(changed=True,failed=failed_var, meta=files)
    
if __name__ == '__main__':
    main()